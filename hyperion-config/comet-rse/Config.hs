-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion
import qualified Hyperion.Slurm as Slurm

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "comet-rse" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/oasis/scratch/comet/rse/temp_project/hyperion")
  { emailAddr = Just "rajeev.erramilli@yale.edu"
  , defaultSbatchOptions = Slurm.defaultSbatchOptions
    { Slurm.partition = Just "shared" }
  }
