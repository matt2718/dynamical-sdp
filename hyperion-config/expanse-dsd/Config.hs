-- To use, create a symlink 'src/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Config where

import           Data.FileEmbed (makeRelativeToProject, strToExp)
import           Hyperion
import qualified Hyperion.Slurm as Slurm

scriptsDir :: FilePath
scriptsDir = $(makeRelativeToProject "expanse-dsd" >>= strToExp)

config :: HyperionConfig
config = (defaultHyperionConfig "/expanse/lustre/scratch/dsd/temp_project")
  { emailAddr             = Just "dsd@caltech.edu"
  , defaultSbatchOptions = Slurm.defaultSbatchOptions
    { Slurm.partition = Just "compute"
    , Slurm.account   = Just "yun124"
    }
  , maxSlurmJobs          = Just 64
  }
