{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedRecordDot #-}

module Projects.DynamicalSdpTest where

import GHC.StaticPtr (StaticPtr)
import qualified Blocks.ScalarBlocks                    as SB
import           Bootstrap.Math.Linear                  (V, toV, toM)
import qualified Bootstrap.Math.Linear                  as L
import           Bounds.Scalars3d.IsingSigEps           (IsingSigEps(..))
import           Control.Monad.Reader                   (local)
import           Data.Aeson                             (FromJSON, ToJSON)
import           Data.Ratio                             (approxRational)
import qualified Data.Set                               as Set
import           Data.Text                              (Text)
import           GHC.TypeNats                           (KnownNat)
import           Hyperion
import           Hyperion.Bootstrap.Bound               (Bound (..), BoundFiles)
import qualified Hyperion.Bootstrap.Bound               as Bound
import           Hyperion.Bootstrap.Main                (unknownProgram)
import           Hyperion.Bootstrap.Params              (blockParamsNmax,
                                                         optimizationParams)
import qualified Hyperion.Log                           as Log
import           Hyperion.Util                          (hour, minute)
import           Numeric.Rounded                        (Rounded,
                                                         RoundingMode (..))
import           Projects.Scalars3d.Defaults            (defaultBoundConfig)
import           Projects.Scalars3d.IsingSigEpsTest2020
import           SDPB                                   (Params (..))
import qualified SDPB                                   as SDPB
import           Blocks.ScalarBlocks               (ScalarBlockParams (..))
import qualified Hyperion.Database                           as DB
import           Data.Typeable                               (Typeable)
import           Projects.SkydivingRun                  (DynamicalSdpOutput(..), 
                                                         DynamicalDBOutput(..),
                                                         defaultDynamicalConfig,
                                                         DynamicalConfig(..),
                                                         BigFloat,
                                                         runDynamicalSdpBound)

srunDynamicalSdpPath :: FilePath
--srunDynamicalSdpPath = "/home/aliu/hyperion-projects/dynamical-sdp/hyperion-config/pi-symmetry-aike/srun_dynamical_sdp.sh" 
srunDynamicalSdpPath = "srun_dynamical_sdp.sh"

dynamicalSdpBoundLoop
  :: forall n a . (KnownNat n, RealFloat a, Show a, ToJSON a, FromJSON a, Typeable a)
  => BoundFiles
  -> DynamicalConfig n a
  -> V n a
  -> (V n a -> Bound Int IsingSigEps)
  -> Int
  -> Job (V n a)
dynamicalSdpBoundLoop boundFiles dynConfig startPoint mkBound numIters = do
    maybeExistDyn <- DB.lookup prevDynamicalMap (numIters::Int)
    case maybeExistDyn of 
      Nothing -> go startPoint numIters dynConfig 
      Just dynDB -> go (extPara dynDB) numIters dynConfig { totalIterationCount = prevIterations dynDB
                                                          , prevGradient = Just (bfgsGradient dynDB)
                                                          , prevHessian = Just (bfgsHessian dynDB)
                                                          , prevExternalStep = Just (extStep dynDB)
                                                          }
  where
    prevDynamicalMap :: DB.KeyValMap (Int) (DynamicalDBOutput n a)
    prevDynamicalMap = DB.KeyValMap "prevDynamicalOutput"
    go p 0 _ = pure p
    go p n dynconfig = do
      dynSdpOut <- runDynamicalSdpBound srunDynamicalSdpPath boundFiles dynconfig p mkBound
      let p' = p + externalParamStep dynSdpOut 
      Log.info "dynamicalSdpBoundLoop: Computed" dynSdpOut
      Log.info "dynamicalSdpBoundLoop: Testing point" p'
      DB.insert prevDynamicalMap (numIters::Int) $ 
                DynamicalDBOutput 
                  { prevIterations = totalIterations dynSdpOut 
                  , bfgsGradient = gradientBFGS dynSdpOut 
                  , bfgsHessian  = hessianBFGS dynSdpOut
                  , extPara = p'
                  , extStep = externalParamStep dynSdpOut
                  }
      let dynconfig' = dynconfig  { totalIterationCount = totalIterations dynSdpOut
                                  , prevExternalStep = Just (externalParamStep dynSdpOut) 
                                  , prevGradient = Just (gradientBFGS dynSdpOut)
                                  , prevHessian  = Just (hessianBFGS dynSdpOut)
                                  --, useExactHessian = (n < 201)
                                  }
      if
        | (SDPB.terminateReason (sdpbOutput dynSdpOut)) == SDPB.UpdateSDPs 
           -> go p' (n-1) dynconfig' 
        | otherwise -> pure p

---------------
-- 1D search --
---------------

-- Search for Minimum --
testDynamicalSdpIsingTheta :: Int -> Job (V 1 (BigFloat 512))
testDynamicalSdpIsingTheta numIters = do
  workDir <- newWorkDir (isingNavBound thetaStart)
  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing} 
  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig thetaStart isingNavBound numIters
  where
    dynConfig = (defaultDynamicalConfig 1 ) { bBoxMax = toV (0.99)
                                            , bBoxMin = toV (0.95)
                                            , useExactHessian = True
                                            , dualityGapUpperLimit = 1e30 
                                            }
    thetaStart = toV 0.95926 
    deltaExts = toV (0.518149, 1.412625)
    nmax = 6

    isingNavBound :: V 1 (BigFloat 512) -> Bound Int IsingSigEps
    isingNavBound (L.V1 theta) = Bound
      { boundKey = isingGFFNavigatorDefaultGaps deltaExts lambda nmax
      , precision = (blockParamsNmax nmax).precision
      , solverParams = (optimizationParams nmax) { precision = 768, writeSolution = Set.fromList [SDPB.DualVector_y, SDPB.PrimalVector_x] }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = Just $ flip approxRational 1e-200 <$> toV (cos theta, sin theta)

testDynamicalSdpIsingThetaStaticPtr :: StaticPtr (Int -> Job (V 1 (BigFloat 512)))
testDynamicalSdpIsingThetaStaticPtr = static testDynamicalSdpIsingTheta

testDynamicalSdpIsingSig :: Int -> Job (V 1 (BigFloat 512))
testDynamicalSdpIsingSig numIters = do
  workDir <- newWorkDir (isingNavBound deltaSigStart)
  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing} 
  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig deltaSigStart isingNavBound numIters
  where
    dynConfig = (defaultDynamicalConfig 1 ) { bBoxMax = toV (0.530)
                                            , bBoxMin = toV (0.515)
                                            , useExactHessian = False
                                            , prevHessian = Just (toM ((1.0)))
                                            }
    deltaSigStart = toV (0.5182) 
    deltaEps =  1.412625
    theta = 0.96980 :: BigFloat 512
    nmax = 6

    isingNavBound :: V 1 (BigFloat 512) -> Bound Int IsingSigEps
    isingNavBound (L.V1 deltaSig) = Bound
      { boundKey = isingGFFNavigatorDefaultGaps (toV ((realToFrac deltaSig),deltaEps)) lambda nmax
      , precision = (blockParamsNmax nmax).precision
      , solverParams = (optimizationParams nmax) { precision = 768 
                                                 , infeasibleCenteringParameter = 0.3 
                                                 , initialMatrixScalePrimal = 1e30 
                                                 , initialMatrixScaleDual = 1e30
                                                 , writeSolution = Set.fromList [SDPB.DualVector_y, SDPB.PrimalVector_x] 
                                                 }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = Just $ flip approxRational 1e-200 <$> toV (cos theta, sin theta)

-- Search for Boundary --
testSearchBoundaryIsingTheta :: Int -> Job (V 1 (BigFloat 512))
testSearchBoundaryIsingTheta numIters = do
  workDir <- newWorkDir (isingNavBound thetaStart)
  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing } 
  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig thetaStart isingNavBound numIters
  where
    dynConfig = (defaultDynamicalConfig 1 ) { bBoxMax = toV (0.99)
                                            , bBoxMin = toV (0.95)
                                            , findBoundaryObjThreshold = Just 1e-20
                                            , useExactHessian = True
                                            , findBoundaryDirection = Just (toV (-1))
                                            }
    thetaStart = toV 0.971  
    deltaExts = toV (0.518149, 1.412625)
    nmax = 6

    isingNavBound :: V 1 (BigFloat 512) -> Bound Int IsingSigEps
    isingNavBound (L.V1 theta) = Bound
      { boundKey = isingGFFNavigatorDefaultGaps deltaExts lambda nmax
      , precision = (blockParamsNmax nmax).precision
      , solverParams = (optimizationParams nmax) { precision = 768, writeSolution = Set.fromList [SDPB.DualVector_y, SDPB.PrimalVector_x] }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = Just $ flip approxRational 1e-200 <$> toV (cos theta, sin theta)


testSearchIsingBoundarySig :: Int -> Job (V 1 (BigFloat 512))
testSearchIsingBoundarySig numIters = do
  workDir <- newWorkDir (isingNavBound thetaStart)
  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing } 
  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig thetaStart isingNavBound numIters
  where
    dynConfig = (defaultDynamicalConfig 1 ) { bBoxMax = toV (0.530)
                                            , bBoxMin = toV (0.515)
                                            , findBoundaryObjThreshold = Just 1e-30
                                            , useExactHessian = False
                                            , prevHessian = Just $ toM ((1.0))
                                            , findBoundaryDirection = Just (toV (-1))
                                            }
    thetaStart = toV 0.5182   
    deltaEps =  1.412625
    theta = 0.96980 :: BigFloat 512
    nmax = 6

    isingNavBound :: V 1 (BigFloat 512) -> Bound Int IsingSigEps
    isingNavBound (L.V1 deltaSig) = Bound
      { boundKey = isingGFFNavigatorDefaultGaps (toV ((realToFrac deltaSig),deltaEps)) lambda nmax
      , precision = (blockParamsNmax nmax).precision
      , solverParams = (optimizationParams nmax) { precision = 768
                                                 , infeasibleCenteringParameter = 0.3
                                                 , initialMatrixScalePrimal = 1e30
                                                 , initialMatrixScaleDual = 1e30
                                                 , writeSolution = Set.fromList [SDPB.DualVector_y, SDPB.PrimalVector_x]
                                                 }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = Just $ flip approxRational 1e-200 <$> toV (cos theta, sin theta)


---------------
-- 2D search --
---------------

-- Search for Minimum --
testDynamicalSdpIsing2dThetaSig :: Int -> Job (V 2 (BigFloat 512))
testDynamicalSdpIsing2dThetaSig numIters = do
  workDir <- newWorkDir (isingNavBound externalStart)
  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing } 
  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig externalStart isingNavBound numIters
  where
    dynConfig = (defaultDynamicalConfig 2) { bBoxMax = toV (0.975, 0.519)
                                           , bBoxMin = toV (0.955, 0.517)
                                           , useExactHessian = True                                           
                                           } 
    externalStart = toV (0.95926, 0.519)
    deltaEps =  1.412625
    nmax = 6

    isingNavBound :: V 2 (BigFloat 512) -> Bound Int IsingSigEps
    isingNavBound (L.V2 theta deltaSig) = Bound
      { boundKey = isingGFFNavigatorDefaultGaps (toV ((realToFrac deltaSig),deltaEps)) lambda nmax
      , precision = (blockParamsNmax nmax).precision
      , solverParams = (optimizationParams nmax) { precision = 768, writeSolution = Set.fromList [SDPB.DualVector_y, SDPB.PrimalVector_x] }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = Just $ flip approxRational 1e-200 <$> toV (cos theta, sin theta)

testDynamicalSdpIsing2dSigEps :: Int -> Job (V 2 (BigFloat 512))
testDynamicalSdpIsing2dSigEps numIters = do
  workDir <- newWorkDir (isingNavBound externalStart)
  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing} 
  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig externalStart isingNavBound numIters
  where
    dynConfig = (defaultDynamicalConfig 2) { bBoxMax = toV (0.530, 1.50)
                                           , bBoxMin = toV (0.517, 1.20)
                                           , prevHessian = Just $ toM ((1.0,0.0),(0.0,1.0))
                                           }
    externalStart = toV (0.517, 1.4) 
    theta = 0.96980 :: BigFloat 512
    nmax = 6

    isingNavBound :: V 2 (BigFloat 512) -> Bound Int IsingSigEps
    isingNavBound (L.V2 deltaSig deltaEps) = Bound
      { boundKey = isingGFFNavigatorDefaultGaps (toV ((realToFrac deltaSig), (realToFrac deltaEps))) lambda nmax
      , precision = (blockParamsNmax nmax).precision
      , solverParams = (optimizationParams nmax) { precision = 768, writeSolution = Set.fromList [SDPB.DualVector_y, SDPB.PrimalVector_x] }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = Just $ flip approxRational 1e-200 <$> toV (cos theta, sin theta)


-- Search for Boundary --
testDynamicalSdpIsing2dBoundaryThetaSig :: Int -> Job (V 2 (BigFloat 512))
testDynamicalSdpIsing2dBoundaryThetaSig numIters = do
  workDir <- newWorkDir (isingNavBound externalStart)
  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing} 
  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig externalStart isingNavBound numIters
  where
    dynConfig = (defaultDynamicalConfig 2) { bBoxMax = toV (0.975, 0.530)
                                           , bBoxMin = toV (0.955, 0.515)
                                           , findBoundaryDirection = Just (toV (-1,0))
                                           , prevHessian = Just $ toM ((1.0,0.0),(0.0,1.0))
                                           }
    externalStart = toV (0.95926,0.519) 
    deltaEps =  1.412625
    nmax = 6

    isingNavBound :: V 2 (BigFloat 512) -> Bound Int IsingSigEps
    isingNavBound (L.V2 theta deltaSig) = Bound
      { boundKey = isingGFFNavigatorDefaultGaps (toV ((realToFrac deltaSig),deltaEps)) lambda nmax
      , precision = (blockParamsNmax nmax).precision
      , solverParams = (optimizationParams nmax) { precision = 768, writeSolution = Set.fromList [SDPB.DualVector_y, SDPB.PrimalVector_x] }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = Just $ flip approxRational 1e-200 <$> toV (cos theta, sin theta)


testDynamicalSdpIsing2dBoundarySigEps :: Int -> Job (V 2 (BigFloat 512))
testDynamicalSdpIsing2dBoundarySigEps numIters = do
  workDir <- newWorkDir (isingNavBound externalStart)
  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing} 
  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig externalStart isingNavBound numIters
  where
    dynConfig = (defaultDynamicalConfig 2) { bBoxMax = toV (0.530, 1.50)
                                           , bBoxMin = toV (0.517, 1.20)
                                           , findBoundaryDirection = Just (toV (0,1))
                                           , prevHessian = Just $ toM ((1.0,0.0),(0.0,1.0))
                                           }
    externalStart = toV (0.517, 1.4)
    theta = 0.96980 :: BigFloat 512
    nmax = 6

    isingNavBound :: V 2 (BigFloat 512) -> Bound Int IsingSigEps
    isingNavBound (L.V2 deltaSig deltaEps) = Bound
      { boundKey = isingGFFNavigatorDefaultGaps (toV ((realToFrac deltaSig), (realToFrac deltaEps))) lambda nmax
      , precision = (blockParamsNmax nmax).precision
      , solverParams = (optimizationParams nmax) { precision = 768, writeSolution = Set.fromList [SDPB.DualVector_y, SDPB.PrimalVector_x] }
      , boundConfig = defaultBoundConfig
      }
      where
        lambda = Just $ flip approxRational 1e-200 <$> toV (cos theta, sin theta)


---------------
-- 3D search --
---------------

-- Search for Minimum --

testDynamicalSdpIsing3dSigEpsTheta :: Int -> Job (V 3 (BigFloat 512))
testDynamicalSdpIsing3dSigEpsTheta numIters = do
  workDir <- newWorkDir (isingNavBound externalStart)
  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing} 
  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig externalStart isingNavBound numIters
  where
    dynConfig = (defaultDynamicalConfig 3) { bBoxMax = toV (0.530, 1.5, 0.99)
                                           , bBoxMin = toV (0.517, 1.2, 0.95)
                                           , useExactHessian = False 
                                           , prevHessian = Just $ toM ((1000000.0,0.0,0.0),(0.0,10000.0,0.0), (0.0,0.0,1000.0))
                                           }
    externalStart = toV (0.519, 1.4, 0.956)  
    nmax = 6

    isingNavBound :: V 3 (BigFloat 512) -> Bound Int IsingSigEps
    isingNavBound (L.V3 deltaSig deltaEps theta) = Bound
      { boundKey = boundKey'{spins = specialSpinSet, blockParams = blockParams'{keptPoleOrder = 12, order = 48}} 
      , precision = (blockParamsNmax nmax).precision
      , solverParams = (optimizationParams nmax) { feasibleCenteringParameter = 0.3, precision = 768, writeSolution = Set.fromList [SDPB.DualVector_y, SDPB.PrimalVector_x] }
      , boundConfig = defaultBoundConfig
      }
      where
        boundKey' = isingGFFNavigatorDefaultGaps (toV ((realToFrac deltaSig), (realToFrac deltaEps))) lambda nmax
        specialSpinSet = [0 .. 20] ++ [49, 52]
        blockParams' = blockParams boundKey'
        lambda = Just $ flip approxRational 1e-200 <$> toV (cos theta, sin theta)

-- Search for Boundary --

testDynamicalSdpIsing3dBoundarySigEpsTheta :: Int -> Job (V 3 (BigFloat 512))
testDynamicalSdpIsing3dBoundarySigEpsTheta numIters = do
  workDir <- newWorkDir (isingNavBound externalStart)
  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir =  Nothing} 
  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig externalStart isingNavBound numIters
  where
    dynConfig = (defaultDynamicalConfig 3 ) { bBoxMax = toV (0.530, 1.5, 0.99)
                                            , bBoxMin = toV (0.517, 1.2, 0.95)
                                            , findBoundaryDirection = Just (toV (1,0,0))
                                            , prevHessian = Just $ toM ((1000000.0,0.0,0.0),(0.0,10000.0,0.0), (0.0,0.0,1000.0))
                                            }
    externalStart = toV (0.519, 1.4, 0.956)  
    nmax = 6

    isingNavBound :: V 3 (BigFloat 512) -> Bound Int IsingSigEps
    isingNavBound (L.V3 deltaSig deltaEps theta) = Bound
      { boundKey = boundKey'{spins = specialSpinSet, blockParams = blockParams'{keptPoleOrder = 12, order = 48}} 
      , precision = (blockParamsNmax nmax).precision
      , solverParams = (optimizationParams nmax) { feasibleCenteringParameter = 0.3, precision = 768, writeSolution = Set.fromList [SDPB.DualVector_y, SDPB.PrimalVector_x] }
      , boundConfig = defaultBoundConfig
      }
      where
        boundKey' = isingGFFNavigatorDefaultGaps (toV ((realToFrac deltaSig), (realToFrac deltaEps))) lambda nmax
        specialSpinSet = [0 .. 20] ++ [49, 52]
        blockParams' = blockParams boundKey'
        lambda = Just $ flip approxRational 1e-200 <$> toV (cos theta, sin theta)


boundsProgram :: Text -> Cluster ()

boundsProgram "testDynamicalSdpIsingTheta" =
  local (setJobType (MPIJob 1 1) . setJobTime (45*minute) . setSlurmPartition "debugq") $ do
  -- result <- remoteEvalJob $ cPtr $ static (testDynamicalSdpIsingTheta 300)
  result <- remoteEvalJob $ cPtr testDynamicalSdpIsingThetaStaticPtr `cAp` cPure 300
  Log.info "This is the result" result

--
boundsProgram "testDynamicalSdpIsingSig" =
  local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ do
  result <- remoteEvalJob $ cPtr $ static (testDynamicalSdpIsingSig 300)
  Log.info "This is the result" result
--
--boundsProgram "testDynamicalSdpBoundaryIsingTheta" =
--  local (setJobType (MPIJob 1 6) . setJobTime (8*hour)) $ do
--  result <- remoteEvalJob $ cPtr $ static (testSearchBoundaryIsingTheta 500)
--  Log.info "This is the result" result
--
boundsProgram "testDynamicalSdpIsingBoundarySig" =
  local (setJobType (MPIJob 1 6) . setJobTime (8*hour)) $ do
  result <- remoteEvalJob $ cPtr $ static (testSearchIsingBoundarySig 500)
  Log.info "This is the result" result
--
--boundsProgram "testDynamicalSdpIsing2dThetaSig" =
--  local (setJobType (MPIJob 1 6) . setJobTime (8*hour)) $ do
--  result <- remoteEvalJob $ cPtr $ static (testDynamicalSdpIsing2dThetaSig 400)
--  Log.info "This is the result" result
--
--boundsProgram "testDynamicalSdpIsing2dSigEps" =
--  local (setJobType (MPIJob 1 6) . setJobTime (4*hour)) $ do
--  result <- remoteEvalJob $ cPtr $ static (testDynamicalSdpIsing2dSigEps 300)
--  Log.info "This is the result" result
--
--boundsProgram "testDynamicalSdpIsing2dBoundaryThetaSig" =
--  local (setJobType (MPIJob 1 6) . setJobTime (4*hour)) $ do
--  result <- remoteEvalJob $ cPtr $ static (testDynamicalSdpIsing2dBoundaryThetaSig 300)
--  Log.info "This is the result" result
--
--boundsProgram "testDynamicalSdpIsing2dBoundarySigEps" =
--  local (setJobType (MPIJob 1 6) . setJobTime (8*hour)) $ do
--  result <- remoteEvalJob $ cPtr $ static (testDynamicalSdpIsing2dBoundarySigEps 300)
--  Log.info "This is the result" result

boundsProgram "testDynamicalSdpIsing3dSigEpsTheta"=
  local (setJobType (MPIJob 1 16) . setJobTime (8*hour)) $ do
  result <- remoteEvalJob $ cPtr $ static (testDynamicalSdpIsing3dSigEpsTheta 300)
  Log.info "This is the result" result

boundsProgram "testDynamicalSdpIsing3dBoundarySigEpsTheta"=
  local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ do
  result <- remoteEvalJob $ cPtr $ static (testDynamicalSdpIsing3dBoundarySigEpsTheta 300)
  Log.info "This is the result" result

boundsProgram p = unknownProgram p

