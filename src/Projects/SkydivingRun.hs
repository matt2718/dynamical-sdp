{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE DeriveDataTypeable #-}

module Projects.SkydivingRun where

import           Bootstrap.Math.Linear                  (V)
import qualified Bootstrap.Math.Linear                  as L
import           Bootstrap.Math.VectorSpace             ((*^))
import           Control.Monad                          (forM_)
import           Control.Monad.IO.Class                 (liftIO)
import           Data.Aeson                             (FromJSON, ToJSON)
import           Data.Attoparsec.Text                   (decimal)
import           Data.Binary                            (Binary)
import qualified Data.Foldable                          as Foldable
import           Data.Maybe                             (fromJust)
import           Data.Proxy                             (Proxy (..))
import           Data.Scientific                        (Scientific, toRealFloat)
import qualified Data.Vector                            as V
import           Linear.V           (V (..))
import           Data.Matrix.Static (Matrix)
import qualified Data.Matrix.Static  as M
import           GHC.Generics                           (Generic)
import           GHC.TypeNats                           (KnownNat, natVal)
import           Hyperion
import           Hyperion.Bootstrap.Bound               (Bound (..), BoundFiles, CanBuildSDP)
import qualified Hyperion.Bootstrap.Bound               as Bound
import qualified Linear.Matrix                          as L
import           Numeric.Rounded                        (Rounded,
                                                         RoundingMode (..))
import           SDPB                                   (Params (..))
import qualified SDPB                                   as SDPB
import           System.Directory                       (createDirectoryIfMissing,
                                                         removePathForcibly)
import           System.FilePath.Posix                  ((</>))
import           System.Process                         (callProcess)

type BigFloat p = Rounded 'TowardZero p

data DynamicalConfig n a = DynamicalConfig
  { externalParamInfinitestimal         :: Scientific
  , centeringRThreshold                 :: Scientific
  , dualityGapUpperLimit                :: Scientific
  , betaScanMin                         :: Scientific
  , betaScanMax                         :: Scientific
  , betaScanStep                        :: Scientific
  , stepMinThreshold                    :: Scientific
  , stepMaxThreshold                    :: Scientific
  , primalDualObjWeight                 :: Scientific
  , maxClimbingSteps                    :: Int 
  , betaClimbing                        :: Scientific
  , navigatorWithLogDetX                :: Bool
  , gradientWithLogDetX                 :: Bool
  , finiteDualityGapTarget              :: Scientific
  , findBoundaryDirection               :: Maybe (V n a)
  , findBoundaryObjThreshold            :: Maybe Scientific
  , useExactHessian                     :: Bool
  , prevExternalStep                    :: Maybe (V n a)
  , prevGradient                        :: Maybe (V n a)
  , prevHessian                         :: Maybe (Matrix n n a)
  , totalIterationCount                 :: Int
  , bBoxMax                             :: V n a
  , bBoxMin                             :: V n a
  }

defaultDynamicalConfig :: (Num a) => Int -> DynamicalConfig n a
defaultDynamicalConfig dim = DynamicalConfig 
                               { externalParamInfinitestimal= 1e-20
                               , centeringRThreshold        = 1e-20
                               , dualityGapUpperLimit       = 0.1
                               , betaScanMin                = 0.1     --Same as sdpb-skydiving default input      
                               , betaScanMax                = 1.01    --Same as sdpb-skydiving default input  
                               , betaScanStep               = 0.1     --Same as sdpb-skydiving default input  
                               , stepMinThreshold           = 0.1     --Same as sdpb-skydiving default input  
                               , stepMaxThreshold           = 0.6     --Same as sdpb-skydiving default input  
                               , primalDualObjWeight        = 0.2     --Same as sdpb-skydiving default input  
                               , maxClimbingSteps           = 1       --Same as sdpb-skydiving default input  
                               , betaClimbing               = 2       --Same as sdpb-skydiving default input  
                               , navigatorWithLogDetX       = False   --Same as sdpb-skydiving default input  
                               , gradientWithLogDetX        = True    --Same as sdpb-skydiving default input 
                               , finiteDualityGapTarget     = 0       --Same as sdpb-skydiving default input 
                               , findBoundaryDirection      = Nothing
                               , findBoundaryObjThreshold   = Nothing 
                               , useExactHessian            = False
                               , prevExternalStep           = Nothing
                               , prevGradient               = Nothing
                               , prevHessian                = Nothing
                               , totalIterationCount        = 0
                               , bBoxMax                    = initVec
                               , bBoxMin                    = initVec
                               } 
                         where
                           initVec = V $ V.fromList $ take dim (repeat 1)
                           initMat = M.fromListsUnsafe $ take dim (repeat $ take dim (repeat 0))
                           
data DynamicalSdpInput n = DynamicalSdpInput
  { params                :: Params
  , sdpDir                :: FilePath
  , newSdpDirs            :: [FilePath]
  , outDir                :: FilePath
  , checkpointDir         :: FilePath
  , initialCheckpointDir  :: Maybe FilePath
  } deriving (Show, Generic, Binary, ToJSON, FromJSON)

data ParamShift n = Plus Int | Minus Int | Sum Int Int
  deriving (Eq, Ord)

paramShiftString :: ParamShift n -> String
paramShiftString = \case
  Plus i  -> "plus_" ++ show i
  Minus i -> "minus_" ++ show i
  Sum i j -> "sum_" ++ show i ++ "_" ++ show j

allParamShifts :: forall n . KnownNat n => Bool -> [ParamShift n]
allParamShifts useExactHess =
  [Plus i | i <- [0 .. n'-1]] ++
  case useExactHess of
    False -> []
    True -> [Minus i | i <- [0 .. n'-1]] ++
             [Sum i j | i <- [0 .. n'-1], j <- [i+1 .. n'-1]]
  where
    n' = fromIntegral (natVal @n Proxy)

paramShiftToVec :: (KnownNat n, Num a) => ParamShift n -> V n a
paramShiftToVec = \case
  Plus  i -> e i
  Minus i -> -e i
  Sum i j -> e i + e j
  where
    e i = L.identity L.! i

applyParamShift :: (KnownNat n, Eq a, Num a) => a -> Maybe (ParamShift n) -> V n a -> V n a
applyParamShift stepSize shift v = (stepSize *^ maybe 0 paramShiftToVec shift) + v

applyBoundingBox :: forall n a.(KnownNat n, Ord a) => V n a -> V n a -> V n a -> V n a 
applyBoundingBox boxMax boxMin v =  V (V.fromList [max (min (boxMax L.! i)  (v L.! i)) (boxMin L.! i) | i<-[0..n'-1]] )
  where  
    n' = fromIntegral (natVal @n Proxy)

    
data DynamicalSdpOutput n a = DynamicalSdpOutput
  { sdpbOutput            :: SDPB.Output
  , externalParamStep     :: V n a
  , totalIterations       :: Int
  , gradientBFGS          :: V n a
  , hessianBFGS           :: Matrix n n a
  , hessianExact          :: Matrix n n a 
  } deriving (Show, Generic, Binary, ToJSON, FromJSON, Functor)

instance (KnownNat p, KnownNat n) => Static (Binary (DynamicalSdpOutput n (Rounded 'TowardZero p))) where
  closureDict = static (\Dict -> Dict) `ptrAp` closureDict @(KnownNat p, KnownNat n)


readTotalIterations :: (Integral a) => FilePath -> IO a
readTotalIterations outDir = SDPB.readParse decimal (outDir </> "iterations.txt")

readExtParamStep :: forall n a . (KnownNat n, RealFloat a) => FilePath -> IO (V n a)
readExtParamStep outDir =
  L.fromRawVector <$>
  SDPB.readParse SDPB.vectorParser (outDir </> "externalParamStep.txt")

readGradientBFGS :: forall n a . (KnownNat n, RealFloat a) => FilePath -> IO (V n a)
readGradientBFGS outDir =
  L.fromRawVector <$>
  SDPB.readParse SDPB.vectorParser (outDir </> "gradient.txt")

readHessianBFGS :: forall n a . (KnownNat n, RealFloat a) => FilePath -> IO (Matrix n n a)
readHessianBFGS outDir =
  M.fromListUnsafe <$> (V.toList 
  <$> SDPB.readParse SDPB.vectorParser (outDir </> "hessBFGS.txt"))

readHessianBFGSpp :: forall n a . (KnownNat n, RealFloat a) => FilePath -> IO (Matrix n n a)
readHessianBFGSpp outDir =
  M.fromListUnsafe <$> (V.toList
  <$> SDPB.readParse SDPB.vectorParser (outDir </> "hessBFGSpp.txt"))


readHessian :: forall n a . (KnownNat n, RealFloat a) => FilePath -> IO (Matrix n n a)
readHessian outDir =
  M.fromListUnsafe <$> (V.toList
  <$> SDPB.readParse SDPB.vectorParser (outDir </> "hessExact.txt"))

readDynamicalOutDir :: (KnownNat n, RealFloat a) => FilePath -> IO (DynamicalSdpOutput n a)
readDynamicalOutDir outDir = DynamicalSdpOutput
  <$> SDPB.readOutFile outDir
  <*> readExtParamStep outDir
  <*> readTotalIterations outDir
  <*> readGradientBFGS outDir
  <*> readHessianBFGS outDir
  <*> readHessian outDir

getDynamicalSdpArgs :: forall n a . (KnownNat n, Show a) => DynamicalSdpInput n -> DynamicalConfig n a -> IO [String]
getDynamicalSdpArgs DynamicalSdpInput{..} DynamicalConfig{..} = do
  paramArgs <- SDPB.getParamArgs params
  newSdpDirsNsv <- SDPB.makeNsvFile newSdpDirs
  pure $
    paramArgs ++
    [ "--sdpDir",            sdpDir
    , "--outDir",            outDir
    , "--checkpointDir",     checkpointDir
    , "--newSdpDirs",        newSdpDirsNsv
    , "--numExternalParams"          , show (natVal @n Proxy)
    , "--externalParamInfinitestimal", show externalParamInfinitestimal
    , "--centeringRThreshold"        , show centeringRThreshold
    , "--dualityGapUpperLimit"       , show dualityGapUpperLimit
    , "--betaScanMin"                , show betaScanMin       
    , "--betaScanMax"                , show betaScanMax          
    , "--betaScanStep"               , show betaScanStep
    , "--stepMinThreshold"           , show stepMinThreshold
    , "--stepMaxThreshold"           , show stepMaxThreshold           
    , "--primalDualObjWeight"        , show primalDualObjWeight          
    , "--maxClimbingSteps"           , show maxClimbingSteps          
    , "--betaClimbing"               , show betaClimbing          
    , "--navigatorWithLogDetX"       , show navigatorWithLogDetX          
    , "--gradientWithLogDetX"        , show gradientWithLogDetX           
    , "--finiteDualityGapTarget"     , show finiteDualityGapTarget          
    , "--totalIterationCount",  show totalIterationCount
    ] 
    ++
    [ "--boundingBoxMax"]  ++ [show x | x <- Foldable.toList (bBoxMax)] 
    ++
    [ "--boundingBoxMin"]  ++ [show x | x <- Foldable.toList (bBoxMin)]  
    ++
    case (findBoundaryDirection,findBoundaryObjThreshold) of
      (Just v, Just thresh)  -> [ "--findBoundaryObjThreshold", show thresh] ++
                                [ "--findBoundaryDirection"]  ++ [ show x | x <- Foldable.toList v] 
      (Just v, Nothing)      -> [ "--findBoundaryDirection"]  ++ [ show x | x <- Foldable.toList v] 
      (Nothing,_) -> []
    ++
    [ "--useExactHessian", show useExactHessian]
    ++
    case (useExactHessian,prevExternalStep, prevHessian) of
      (True, _, _)               ->  [] 
      (False,Nothing, Nothing)   -> []
      (False,Nothing, (Just mt))  ->  [ "--prevHessianBFGS"] ++ [show x | x <- Foldable.toList mt]
      (False,(Just _), _) ->  [ "--prevExternalStep"] ++ [show x | x <- Foldable.toList (fromJust prevExternalStep)] ++
                           [ "--prevGradientBFGS"] ++ [show x | x <- Foldable.toList (fromJust prevGradient)] ++
                           [ "--prevHessianBFGS"] ++ [show x | x <- Foldable.toList (fromJust prevHessian)] 
    ++
    case initialCheckpointDir of
      Just dir -> ["--initialCheckpointDir", dir]
      Nothing  -> []
   
runDynamicalSdp :: (KnownNat n, RealFloat a, Show a) => FilePath -> DynamicalSdpInput n -> DynamicalConfig n a -> IO (DynamicalSdpOutput n a)
runDynamicalSdp dynamicalSdpExecutable input dynconfig= do
  args <- getDynamicalSdpArgs input dynconfig
  callProcess dynamicalSdpExecutable args
  readDynamicalOutDir (outDir input)


-- | Build an SDP at 'centralPoint', and other SDPs arranged around
-- 'centralPoint' by a distance 'stepSize'. Run dynamical_sdp on the
-- group of SDPs.
runDynamicalSdpBound
  :: (RealFloat a, Show a, KnownNat n, CanBuildSDP b)
  => FilePath 
  -> BoundFiles
  -> DynamicalConfig n a
  -> V n a
  -> (V n a -> Bound Int b)
  -> Job (DynamicalSdpOutput n a)
runDynamicalSdpBound dynScript boundFiles dynConfig centralPoint mkBound = do
  -- | Delete the sdpDir. We may want to re-use blocks and json files
  -- from previous computations, so we leave those alone.
  liftIO $ removePathForcibly (Bound.sdpDir boundFiles)
  liftIO $ createDirectoryIfMissing True (Bound.sdpDir boundFiles)
  forM_ maybeShifts $ \mShift ->
    Bound.makeSDPDir
    (mkBound (applyParamShift stepsize mShift centralPoint))
    (boundFilesShift mShift)
  liftIO $ runDynamicalSdp srunDynPath dynamicalSdpInput dynConfig 
  where
    stepsize = toRealFloat $ externalParamInfinitestimal dynConfig
    maybeShifts = Nothing : map Just (allParamShifts $ useExactHessian dynConfig)

    newSdpDir Nothing      = Bound.sdpDir boundFiles </> "zero"
    newSdpDir (Just shift) = Bound.sdpDir boundFiles </> paramShiftString shift

    boundFilesShift maybeShift = boundFiles { Bound.sdpDir = newSdpDir maybeShift }

    srunDynPath = Bound.scriptsDir (Bound.boundConfig (mkBound undefined)) </> dynScript  
    dynamicalSdpInput = DynamicalSdpInput
      { params               = solverParams (mkBound centralPoint)
      , sdpDir               = newSdpDir Nothing
      , newSdpDirs           = map (newSdpDir . Just)  
                               (allParamShifts $ useExactHessian dynConfig)
      , outDir               = Bound.outDir boundFiles
      , checkpointDir        = Bound.checkpointDir boundFiles
      , initialCheckpointDir = Bound.initialCheckpointDir boundFiles
      }

data DynamicalDBOutput n a = DynamicalDBOutput
  { prevIterations :: Int 
  , bfgsGradient :: V n a
  , bfgsHessian :: Matrix n n a
  , extPara :: V n a
  , extStep :: V n a
  } 
  deriving (Show, Generic, Binary, ToJSON, FromJSON, Functor) 

--data SkydivingOutput = SkydivingOutput
--  { terminateReason :: SDPB.TerminateReason
--  , primalObjective :: Scientific
--  , dualObjective   :: Scientific
--  , dualityGap      :: Scientific
--  , primalError     :: Scientific
--  , dualError       :: Scientific
--  , dualStepSize    :: Scientific
--  , primalStepSize  :: Scientific
--  , bfgsHessianUpdated :: Scientific
--  , navigatorValue  :: Scientific
--  , findMinimumQ    :: Scientific
--  , beta            :: Scientific
--  , mulogdetX       :: Scientific
--  , climbedQ        :: Scientific
--  , runtime         :: NominalDiffTime
--  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)
--
--skydivingOutputParser :: Parser SkydivingOutput
--skydivingOutputParser = SkydivingOutput
--  <$> val "terminateReason" (SDPB.readTerminateReason =<< quotedText)
--  <*> val "primalObjective" scientific
--  <*> val "dualObjective"   scientific
--  <*> val "dualityGap"      scientific
--  <*> val "primalError"     scientific
--  <*> val "dualError"       scientific
--  <*> val "dualStepSize"      scientific
--  <*> val "primalStepSize"    scientific
--  <*> val ("bfgsHessianUpdated" <|> "BFGSHessianUpdated") scientific
--  <*> val ("navigatorValue" <|> "NavigatorValue")    scientific
--  <*> val "findMinimumQ"       scientific
--  <*> val "beta"               scientific
--  <*> val "mulogdetX"          scientific
--  <*> val "climbedQ"           scientific
--  <*> val ("runtime" <|> "Solver runtime") (fmap realToFrac double)
--  where
--    val :: Parser Text -> Parser a -> Parser a
--    val k v = skipSpace >> k >> skipSpace >> "=" >> skipSpace >> v <* ";"
--    quotedText = "\"" *> takeWhile1 (/= '"') <* "\""


