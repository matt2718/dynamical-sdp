{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedRecordDot #-}

module Projects.DynamicalIsing where

import qualified Blocks.ScalarBlocks                    as SB
import           Bootstrap.Math.Linear                  (V, toV, diagonalMatrix)
import qualified Bootstrap.Math.Linear                  as L
import           Bounds.Scalars3d.IsingSigEpsEpsP       (IsingSigEpsEpsP(..))
import           Control.Monad.Reader                   (local)
import           Data.Aeson                             (FromJSON, ToJSON)
import           Data.Ratio                             (approxRational)
import qualified Data.Set                               as Set
import           Data.Text                              (Text)
import           GHC.TypeNats                           (KnownNat)
import           Hyperion
import           Hyperion.Bootstrap.Bound               (Bound (..), BoundFiles)
import qualified Hyperion.Bootstrap.Bound               as Bound
import           Hyperion.Bootstrap.Main                (unknownProgram)
import           Hyperion.Bootstrap.Params              (blockParamsNmax,
                                                         optimizationParams)
import qualified Hyperion.Log                           as Log
import           Hyperion.Util                          (hour)
import           Projects.Scalars3d.Defaults            (defaultBoundConfig)
import           Projects.Scalars3d.IsingSEEPTest2023
import           SDPB                                   (Params (..))
import qualified SDPB                                   as SDPB
import           Blocks.ScalarBlocks                    (ScalarBlockParams (..))
import qualified Hyperion.Database                      as DB
import           Data.Typeable                          (Typeable)
import           Projects.SkydivingRun                  (DynamicalSdpOutput(..), 
                                                         DynamicalDBOutput(..), 
                                                         defaultDynamicalConfig, 
                                                         DynamicalConfig(..),
                                                         BigFloat, 
                                                         runDynamicalSdpBound) 

srunDynamicalSdpPath :: FilePath
srunDynamicalSdpPath = "/home/aliu7/dynamical-sdp/hyperion-config/caltech-hpc-aike/srun_dynamical_sdp_test.sh"

dynamicalSdpBoundLoop
  :: forall n a . (KnownNat n, RealFloat a, Show a, ToJSON a, FromJSON a, Typeable a)
  => BoundFiles
  -> DynamicalConfig n a
  -> V n a
  -> (V n a -> Bound Int IsingSigEpsEpsP)
  -> Int
  -> Job (V n a)
dynamicalSdpBoundLoop boundFiles dynConfig startPoint mkBound numIters = do
    maybeExistDyn <- DB.lookup prevDynamicalMap (numIters::Int)
    case maybeExistDyn of 
      Nothing -> go startPoint numIters dynConfig 
      Just dynDB -> go (extPara dynDB) numIters dynConfig { totalIterationCount = prevIterations dynDB
                                                          , prevGradient = Just $ bfgsGradient dynDB
                                                          , prevHessian = Just $ bfgsHessian dynDB
                                                          , prevExternalStep = Just $ extStep dynDB
                                                          }
  where
    prevDynamicalMap :: DB.KeyValMap (Int) (DynamicalDBOutput n a)
    prevDynamicalMap = DB.KeyValMap "prevDynamicalOutput"
    go p 0 _ = pure p
    go p n dynconfig = do
      dynSdpOut <- runDynamicalSdpBound srunDynamicalSdpPath boundFiles dynconfig p mkBound
      let p' = p + externalParamStep dynSdpOut 
      Log.info "dynamicalSdpBoundLoop: Computed" dynSdpOut
      Log.info "dynamicalSdpBoundLoop: Testing point" p'
      DB.insert prevDynamicalMap (numIters::Int) $ 
                DynamicalDBOutput 
                  { prevIterations = totalIterations dynSdpOut 
                  , bfgsGradient = gradientBFGS dynSdpOut 
                  , bfgsHessian  = hessianBFGS dynSdpOut
                  , extPara = p'
                  , extStep = externalParamStep dynSdpOut
                  }
      let dynconfig' = dynconfig  { totalIterationCount = totalIterations dynSdpOut
                                  , prevExternalStep = Just $ externalParamStep dynSdpOut 
                                  , prevGradient = Just $  gradientBFGS dynSdpOut
                                  , prevHessian  = Just $ hessianBFGS dynSdpOut
                                  , useExactHessian = False --(n `mod` 20 == 1)
                                  }
      if
        | (SDPB.terminateReason (sdpbOutput dynSdpOut)) == SDPB.UpdateSDPs 
           -> go p' (n-1) dynconfig' 
        | otherwise -> pure p

{-

---------------
-- 3D search --
---------------
testDynamicalSdpIsingSEEP1d :: Int -> Job (V 1 (BigFloat 512))
testDynamicalSdpIsingSEEP1d numIters = do
  workDir <- newWorkDir (isingNavBound externalStart)
  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing} 
  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig externalStart isingNavBound numIters
  where
    dynConfig = (defaultDynamicalConfig 1) { bBoxMax = toV (0.530)
                                           , bBoxMin = toV (0.517)
                                           , prevHessian =Just $ diagonalMatrix (toV (100.0))
                                           }
    externalStart = toV (0.51803)
    deltaSigM = 0.51803
    deltaEps = 1.412625
    deltaEpsP = 3 
    mLambda = Just $ toV (-0.254569, -0.11829, -0.501378, -0.818389, 0.123, -0.231)
  --  $ flip approxRational 1e-200 <$> 
  --                  toV ( cos 1.81963, 
  --                        (sin 1.81963)*(cos 1.68914), 
  --                        (sin 1.81963)*(sin 1.68914)*(cos 2.0990), 
  --                        (sin 1.81963)*(sin 1.68914)*(sin 2.0990)*(cos 2.8320),
  --                        (sin 1.81963)*(sin 1.68914)*(sin 2.0990)*(sin 2.8320)*(cos 1.0815), 
  --                        (sin 1.81963)*(sin 1.68914)*(sin 2.0990)*(sin 2.8320)*(sin 1.0815)
  --                      )
    nmax = 6
    isingNavBound :: V 1 (BigFloat 512) -> Bound Int IsingSigEpsEpsP 
    isingNavBound dimV = Bound
      { boundKey = boundKey'
      , precision = (blockParamsNmax nmax).precision
      , solverParams = (optimizationParams nmax) { precision = 640 }
      , boundConfig = defaultBoundConfig
      }
      where
        (deltaSig) = L.fromV dimV 
        boundKey' = isingSEEPGFFNavigatorDefaultGaps (toV (deltaSigM,deltaEps, deltaEpsP)) mLambda nmax 

 
-- Search for Minimum --

testDynamicalSdpIsingSEEP3d :: Int -> Job (V 3 (BigFloat 512))
testDynamicalSdpIsingSEEP3d numIters = do
  workDir <- newWorkDir (isingNavBound externalStart)
  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing} 
  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig externalStart isingNavBound numIters
  where
    dynConfig = (defaultDynamicalConfig 3) { bBoxMax = toV (0.530, 1.5, 5)
                                           , bBoxMin = toV (0.517, 1.2, 1)
                                           , prevHessian = Just $ diagonalMatrix (toV (100.0, 100.0,100.0))
                                           }
    externalStart = toV (0.51803, 1.412625, 3) 
    mLambda = Just $ toV (-0.254569, -0.11829, -0.501378, -0.818389, 0.123, -0.231)
  --  $ flip approxRational 1e-200 <$> 
  --                  toV ( cos 1.81963, 
  --                        (sin 1.81963)*(cos 1.68914), 
  --                        (sin 1.81963)*(sin 1.68914)*(cos 2.0990), 
  --                        (sin 1.81963)*(sin 1.68914)*(sin 2.0990)*(cos 2.8320),
  --                        (sin 1.81963)*(sin 1.68914)*(sin 2.0990)*(sin 2.8320)*(cos 1.0815), 
  --                        (sin 1.81963)*(sin 1.68914)*(sin 2.0990)*(sin 2.8320)*(sin 1.0815)
  --                      )
    nmax = 6
    isingNavBound :: V 3 (BigFloat 512) -> Bound Int IsingSigEpsEpsP 
    isingNavBound dimV = Bound
      { boundKey = boundKey'
      , precision = (blockParamsNmax nmax).precision
      , solverParams = (optimizationParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
      where
        (deltaSig, deltaEps, deltaEpsP) = L.fromV dimV 
        boundKey' = isingSEEPGFFNavigatorDefaultGaps (toV (realToFrac deltaSig, realToFrac deltaEps, realToFrac deltaEpsP)) mLambda nmax 

    


testDynamicalSdpIsingSEEP9d :: Int -> Job (V 8 (BigFloat 512))
testDynamicalSdpIsingSEEP9d numIters = do
  workDir <- newWorkDir (isingNavBound externalStart)
  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir = Nothing} 
  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig externalStart isingNavBound numIters
  where
    dynConfig = (defaultDynamicalConfig 8) { bBoxMax = toV (0.530, 1.5, 5, 3, 3, 3, 3, 3)
                                           , bBoxMin = toV (0.517, 1.2, 1, -3, -3, -3, -3, -3)
                                           , prevHessian = Just $ diagonalMatrix (toV (100.0, 100.0,100.0,100.0,100.0,100.0,100.0,100.0))
                                           }
    externalStart = toV (0.51803, 1.412625, 3, 1.81963, 1.68914, 2.0990, 2.8320, 1.0815)  
    nmax = 6
    isingNavBound :: V 8 (BigFloat 512) -> Bound Int IsingSigEpsEpsP 
    isingNavBound dimlambdaV = Bound
      { boundKey = boundKey'{spins = specialSpinSet, blockParams = blockParams'{keptPoleOrder = 12, order = 48}} 
      , precision = (blockParamsNmax nmax).precision
      , solverParams = (optimizationParams nmax) { feasibleCenteringParameter = 0.3, precision = 768, writeSolution = Set.fromList [SDPB.DualVector_y, SDPB.PrimalVector_x] }
      , boundConfig = defaultBoundConfig
      }
      where
        (deltaSig, deltaEps, deltaEpsP, theta1, theta2, theta3, theta4, theta5) = L.fromV dimlambdaV 
        boundKey' = isingSEEPGFFNavigatorDefaultGaps (toV (realToFrac deltaSig, realToFrac deltaEps, realToFrac deltaEpsP)) mLambda nmax 
        specialSpinSet = [0 .. 20] ++ [49, 52]
        blockParams' = blockParams boundKey'
        mLambda = Just $ flip approxRational 1e-200 <$> 
                        toV ( cos theta1, 
                              (sin theta1)*(cos theta2), 
                              (sin theta1)*(sin theta2)*(cos theta3), 
                              (sin theta1)*(sin theta2)*(sin theta3)*(cos theta4),
                              (sin theta1)*(sin theta2)*(sin theta3)*(sin theta4)*(cos theta5), 
                              (sin theta1)*(sin theta2)*(sin theta3)*(sin theta4)*(sin theta5)
                            )

-- Search for Boundary --

--testDynamicalSdpIsing3dBoundarySigEpsTheta :: Int -> Job (V 3 (BigFloat 512))
--testDynamicalSdpIsing3dBoundarySigEpsTheta numIters = do
--  workDir <- newWorkDir (isingNavBound externalStart)
--  let isingNavBoundFiles  = (Bound.defaultBoundFiles workDir) {Bound.initialCheckpointDir =  Nothing} 
--  dynamicalSdpBoundLoop isingNavBoundFiles dynConfig externalStart isingNavBound numIters
--  where
--    dynConfig = (defaultDynamicalConfig 3 ) { bBoxMax = toV (0.530, 1.5, 0.99)
--                                            , bBoxMin = toV (0.517, 1.2, 0.95)
--                                            , findBoundaryDirection = Just (toV (1,0,0))
--                                            , prevHessian = toM ((1000000.0,0.0,0.0),(0.0,10000.0,0.0), (0.0,0.0,1000.0))
--                                            }
--    externalStart = toV (0.519, 1.4, 0.956)  
--    nmax = 6
--
--    isingNavBound :: V 3 (BigFloat 512) -> Bound Int IsingSigEps
--    isingNavBound (L.V3 deltaSig deltaEps theta) = Bound
--      { boundKey = boundKey'{spins = specialSpinSet, blockParams = blockParams'{keptPoleOrder = 12, order = 48}} 
--      , precision = (blockParamsNmax nmax).precision
--      , solverParams = (optimizationParams nmax) { feasibleCenteringParameter = 0.3, precision = 768, writeSolution = Set.fromList [SDPB.DualVector_y, SDPB.PrimalVector_x] }
--      , boundConfig = defaultBoundConfig
--      }
--      where
--        boundKey' = isingGFFNavigatorDefaultGaps (toV ((realToFrac deltaSig), (realToFrac deltaEps))) lambda nmax
--        specialSpinSet = [0 .. 20] ++ [49, 52]
--        blockParams' = blockParams boundKey'
--        lambda = Just $ flip approxRational 1e-200 <$> toV (cos theta, sin theta)


boundsProgram :: Text -> Cluster ()
boundsProgram "testDynamicalSdpIsingSEEP1d"=
  local (setJobType (MPIJob 1 1) . setJobTime (4*hour)) $ do
  result <- remoteEvalJob $ cPtr $ static (testDynamicalSdpIsingSEEP1d 300)
  Log.info "This is the result" result

boundsProgram "testDynamicalSdpIsingSEEP3d"=
  local (setJobType (MPIJob 1 1) . setJobTime (4*hour)) $ do
  result <- remoteEvalJob $ cPtr $ static (testDynamicalSdpIsingSEEP3d 300)
  Log.info "This is the result" result

boundsProgram "testDynamicalSdpIsingSEEP9d"=
  local (setJobType (MPIJob 1 8) . setJobTime (4*hour)) $ do
  result <- remoteEvalJob $ cPtr $ static (testDynamicalSdpIsingSEEP9d 300)
  Log.info "This is the result" result

--boundsProgram "testDynamicalSdpIsing3dBoundarySigEpsTheta"=
--  local (setJobType (MPIJob 1 8) . setJobTime (8*hour)) $ do
--  result <- remoteEvalJob $ cPtr $ static (testDynamicalSdpIsing3dBoundarySigEpsTheta 300)
--  Log.info "This is the result" result

-}

boundsProgram p = unknownProgram p

