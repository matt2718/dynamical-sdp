# About 

An experiment where we allow the SDP to be dynamical during the
solution process.

Note: this is everyday working code, and it may change without notice.

# Usage

- Before compiling, you must create `hyperion-config/src/Config.hs`
  and prepare the appropriate scripts so that `fermions-3d` can use
  `sdpb` and other programs on your machine.  The recommended way to
  do this is:

      cp -r hyperion-config/caltech-hpc-dsd hyperion-config/my-special-machine
      # edit 'hyperion-config/my-special-machine/Config.hs' as necessary
      # edit the scripts in 'hyperion-config/my-special-machine' as necessary
      mkdir hyperion-config/src
      cd hyperion-config/src
      ln -s ../my-special-machine/Config.hs Config.hs

- For `hyperion` to work properly, your nodes should be able to `ssh` into
  other nodes without a password promt or any other interactive steps. This might
  require you to set up a key pair without a passphrase. Also see the [documentation
  for SSHCommand](https://davidsd.github.io/hyperion/hyperion/Hyperion-WorkerCpuPool.html#t:SSHCommand)
  for details on how `ssh` is called. You might need to change it in your `Config.hs`
  file (described above).
