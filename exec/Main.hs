module Main where

import           Config                         (config)
import           Hyperion.Bootstrap.Main        (hyperionBootstrapMain,
                                                 tryAllPrograms)
import qualified Projects.DynamicalSdpTest
import qualified Projects.DynamicalIsing

main :: IO ()
main = hyperionBootstrapMain config $
  tryAllPrograms
  [  Projects.DynamicalSdpTest.boundsProgram
  ,  Projects.DynamicalIsing.boundsProgram
  ]
